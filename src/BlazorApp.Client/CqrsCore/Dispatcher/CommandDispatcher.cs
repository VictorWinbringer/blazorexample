﻿using BlazorApp.Client.TodoModule.Domain.UseCases.Commands;
using BlazorApp.Client.TodoModule.Domain.UseCases.Dispatcher.Exceptions;
using BlazorApp.Client.TodoModule.Domain.UseCases.Handers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Dispatcher
{
    public interface ICommandDispatcher
    {
        Task Execute<TCommand>(TCommand command) where TCommand : ICommand;
    }

    internal sealed class CommandDispatcher : ICommandDispatcher
    {
        private readonly IServiceProvider _serviceProvider;

        public CommandDispatcher(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public Task Execute<TCommand>(TCommand command) where TCommand : ICommand
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            var handler = (ICommandHandler<TCommand>)_serviceProvider.GetService(typeof(ICommandHandler<TCommand>));

            if (handler == null) throw new CommandHandlerNotFoundException(typeof(TCommand));

            return handler.Execute(command);
        }
    }
}
