﻿using BlazorApp.Client.TodoModule.Domain.UseCases.Dispatcher.Exceptions;
using BlazorApp.Client.TodoModule.Domain.UseCases.Handers;
using BlazorApp.Client.TodoModule.Domain.UseCases.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Dispatcher
{
    public interface IQueryDispatcher
    {
        Task<TResult> Execute<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>;
    }

    internal sealed class QueryDispatcher : IQueryDispatcher
    {
        private readonly IServiceProvider _serviceProvider;

        public QueryDispatcher(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Task<TResult> Execute<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>
        {
            if (query == null) throw new ArgumentNullException("query");

            var handler = (IQueryHandler<TQuery, TResult>)_serviceProvider.GetService(typeof(IQueryHandler<TQuery, TResult>));

            if (handler == null) throw new QueryHandlerNotFoundException(typeof(TQuery));

            return handler.Execute(query);
        }
    }
}
