using Blazor.Extensions.Storage;
using BlazorApp.Client.Domain;
using BlazorApp.Client.Infrastructure;
using BlazorApp.Client.TodoModule.Domain.UseCases.Dispatcher;
using BlazorApp.Client.TodoModule.Domain.UseCases.Handers;
using BlazorApp.Client.TodoModule.Domain.UseCases.Handers.QueryHandlers;
using BlazorApp.Client.TodoModule.Domain.UseCases.Queries;
using Microsoft.AspNetCore.Components.Builder;
using Microsoft.Extensions.DependencyInjection;
using BlazorApp.Client.TodoModule.Domain.UseCases.Handers;
using System.Collections.Generic;
using BlazorApp.Client.TodoModule.Domain.UseCases.Commands;
using BlazorApp.Client.TodoModule.Domain.UseCases.Handers.CommandHandlers;

namespace BlazorApp.Client
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddStorage();
            services.AddSingleton<ITodoRepository, TodoRepository>();
            services.AddSingleton<ICommandDispatcher, CommandDispatcher>();
            services.AddSingleton<IQueryDispatcher, QueryDispatcher>();
            services.AddSingleton<IQueryHandler<GetAll, List<TodoItem>>, GetAllHandler>();
            services.AddSingleton<IQueryHandler<GetById, TodoItem>, GetByIdHandler>();
            services.AddSingleton<ICommandHandler<Add>, AddHandler>();
            services.AddSingleton<ICommandHandler<Remove>, RemoveHandler>();
            services.AddSingleton<ICommandHandler<Update>, UpdateHandler>();
        }

        public void Configure(IComponentsApplicationBuilder app)
        {
            app.AddComponent<App>("app");
        }
    }
}
