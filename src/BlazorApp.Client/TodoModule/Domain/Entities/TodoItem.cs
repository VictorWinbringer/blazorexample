﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.Domain
{
    public sealed class TodoItem
    {
        public TodoItem(Guid id, string name, bool isComplite, DateTime created)
        {
            if (id == Guid.Empty)
                throw new ArgumentException($"{nameof(id)} is empty");
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException($"{nameof(name)} is empty");
            Id = id;
            Name = name;
            IsComplite = isComplite;
            Created = created;
        }

        public TodoItem(string name) : this(Guid.NewGuid(), name, false, DateTime.UtcNow) { }

        public Guid Id { get; }
        public string Name { get; private set; }
        public bool IsComplite { get; private set; }
        public DateTime Created { get; }

        public void Rename(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException($"{nameof(name)} is empty");
            Name = name;
        }

        public void MakeComplite()
        {
            IsComplite = true;
        }

        public void MakeUncomplite()
        {
            IsComplite = false;
        }
    }
}
