﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Commands
{
    public class Add:ICommand
    {
        public Add(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
