﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Commands
{
    public class Remove:ICommand
    {
        public Remove(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
