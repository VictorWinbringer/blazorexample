﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Commands
{
    public class Update:ICommand
    {
        public Update(Guid id, string name, bool isComplite)
        {
            Id = id;
            Name = name;
            IsComplite = isComplite;
        }

        public Guid Id { get; }
        public string Name { get; }
        public bool IsComplite { get; }
    }
}
