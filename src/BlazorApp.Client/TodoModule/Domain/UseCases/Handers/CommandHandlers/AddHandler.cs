﻿using BlazorApp.Client.Domain;
using BlazorApp.Client.TodoModule.Domain.UseCases.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Handers.CommandHandlers
{
    public class AddHandler : ICommandHandler<Add>
    {
        private readonly ITodoRepository _todoRepository;

        public AddHandler(ITodoRepository todoRepository)
        {
            _todoRepository = todoRepository;
        }
        public Task Execute(Add command)
        {
            var todo = new TodoItem(command.Name);
            return _todoRepository.Add(todo);
        }
    }
}
