﻿using BlazorApp.Client.Domain;
using BlazorApp.Client.TodoModule.Domain.UseCases.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Handers.CommandHandlers
{
    public class RemoveHandler : ICommandHandler<Remove>
    {
        private readonly ITodoRepository _todoRepository;

        public RemoveHandler(ITodoRepository todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public Task Execute(Remove command)
        {
            return _todoRepository.Remove(command.Id);
        }
    }
}
