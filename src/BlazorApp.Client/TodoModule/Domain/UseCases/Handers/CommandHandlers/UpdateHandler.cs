﻿using BlazorApp.Client.Domain;
using BlazorApp.Client.TodoModule.Domain.UseCases.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Handers.CommandHandlers
{
    public class UpdateHandler : ICommandHandler<Update>
    {
        private readonly ITodoRepository _todoRepository;

        public UpdateHandler(ITodoRepository todoRepository)
        {
            _todoRepository = todoRepository;
        }
        public async Task Execute(Update command)
        {
            var todo = await _todoRepository.Get(command.Id);
            if (todo == null)
                throw new ArgumentException($"todo whith id {command.Id} not found");
            if (command.IsComplite)
            {
                todo.MakeComplite();
            }
            else
            {
                todo.MakeUncomplite();
            }
            todo.Rename(command.Name);
            await _todoRepository.Update(todo);
        }
    }
}
