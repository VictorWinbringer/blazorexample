﻿using BlazorApp.Client.Domain;
using BlazorApp.Client.TodoModule.Domain.UseCases.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Handers.QueryHandlers
{
    public class GetAllHandler : IQueryHandler<GetAll, List<TodoItem>>
    {
        private readonly ITodoRepository _todoRepository;

        public GetAllHandler(ITodoRepository todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public Task<List<TodoItem>> Execute(GetAll query)
        {
            return _todoRepository.GetAll();
        }
    }
}
