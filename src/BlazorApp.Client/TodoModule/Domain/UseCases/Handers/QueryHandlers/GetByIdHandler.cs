﻿using BlazorApp.Client.Domain;
using BlazorApp.Client.TodoModule.Domain.UseCases.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Handers.QueryHandlers
{
    public class GetByIdHandler : IQueryHandler<GetById, TodoItem>
    {
        private readonly ITodoRepository _todoRepository;

        public GetByIdHandler(ITodoRepository todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public Task<TodoItem> Execute(GetById query)
        {
            return _todoRepository.Get(query.Id);
        }
    }
}
