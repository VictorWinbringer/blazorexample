﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.Domain
{
    public interface ITodoRepository
    {
        Task Add(TodoItem todo);
        Task<TodoItem> Get(Guid id);
        Task<List<TodoItem>> GetAll();
        Task Remove(Guid id);
        Task Update(TodoItem item);
    }
}
