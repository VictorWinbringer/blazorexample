﻿using BlazorApp.Client.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Queries
{
    public class GetAll:IQuery<List<TodoItem>>
    {
    }
}
