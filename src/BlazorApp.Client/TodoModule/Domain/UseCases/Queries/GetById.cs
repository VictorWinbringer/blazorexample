﻿using BlazorApp.Client.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Domain.UseCases.Queries
{
    public class GetById : IQuery<TodoItem>
    {
        public Guid Id { get; }

        public GetById(Guid id)
        {
            Id = id;
        }
    }
}
