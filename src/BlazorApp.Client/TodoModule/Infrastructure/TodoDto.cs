﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.Infrastructure
{
    public class TodoDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsComplite { get; set; }
        public DateTime Created { get; set; }
    }
}
