﻿using BlazorApp.Client.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blazor.Extensions.Storage;
using BlazorApp.Client.Pages;

namespace BlazorApp.Client.Infrastructure
{
    internal sealed class TodoRepository : ITodoRepository
    {
        private readonly LocalStorage _localStorage;
        private const string KEY = "TodoItems";
        public TodoRepository(LocalStorage localStorage)
        {
            _localStorage = localStorage;
        }

        public async Task Add(TodoItem todo)
        {
            var items = await Read();
            items.Add(new TodoDto { Id = todo.Id, IsComplite = todo.IsComplite, Name = todo.Name, Created=todo.Created });
            await Save(items);
        }

        public async Task<TodoItem> Get(Guid id)
        {
            var items = await Read();
            var todo = items.FirstOrDefault(t => t.Id == id);
            if (todo == null)
                return null;
            return new TodoItem(todo.Id, todo.Name, todo.IsComplite,todo.Created);
        }

        public async Task<List<TodoItem>> GetAll()
        {
            var items = await Read();
            return items.Select(t => new TodoItem(t.Id, t.Name, t.IsComplite,t.Created))
                .ToList();
        }

        public async Task Remove(Guid id)
        {
            var items = await Read();
            items.RemoveAll(t => t.Id == id);
            await Save(items);
        }

        public async Task Update(TodoItem item)
        {
            var items = await Read();
            var todo = items.FirstOrDefault(t => t.Id == item.Id);
            if (todo == null)
                return;
            todo.Name = item.Name;
            todo.IsComplite = item.IsComplite;
            await Save(items);
        }

        private Task Save(List<TodoDto> items)
        {
            return _localStorage.SetItem(KEY, items);
        }

        private async Task<List<TodoDto>> Read()
        {
            var items = await _localStorage.GetItem<List<TodoDto>>(KEY);
            return items ?? new List<TodoDto>();
        }
    }
}
