﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlazorApp.Client.Presentation
{
    public sealed class TodoDto
    {
        public Guid Id { get; set; }
        [Required]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Название должно содержать от 3 до 255 символов")]
        public string Name { get; set; }
        public bool IsComplite { get; set; }
        public DateTime Created { get; set; }
    }
}
