﻿using BlazorApp.Client.Domain;
using BlazorApp.Client.Infrastructure;
using BlazorApp.Client.Shared;
using BlazorApp.Client.TodoModule.Domain.UseCases.Commands;
using BlazorApp.Client.TodoModule.Domain.UseCases.Dispatcher;
using BlazorApp.Client.TodoModule.Domain.UseCases.Queries;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Client.TodoModule.Presentation
{
    public class DeleteTodoViewModel : ComponentBase
    {
        [Parameter]
        private Guid Id { get; set; }

        [Inject]
        public ICommandDispatcher CommandDispatcher { get; set; }

        [Inject]
        public IQueryDispatcher QueryDispatcher { get; set; }

        [Inject]
        public IUriHelper UriHelper { get; set; }

        public TodoDto Todo { get; set; }

        protected override async Task OnInitAsync()
        {
            var todo = await QueryDispatcher.Execute<GetById,TodoItem>(new GetById(Id));
            if (todo != null)
                Todo = new TodoDto { Id = todo.Id, IsComplite = todo.IsComplite, Name = todo.Name, Created = todo.Created };
            await base.OnInitAsync();
        }

        public async Task Delete()
        {
            if (Todo != null)
                await CommandDispatcher.Execute(new Remove(Todo.Id));
            Todo = null;
            UriHelper.NavigateTo("/todo");
        }
    }
}
