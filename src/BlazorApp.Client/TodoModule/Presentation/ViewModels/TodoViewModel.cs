﻿using BlazorApp.Client.Domain;
using BlazorApp.Client.TodoModule.Domain.UseCases.Commands;
using BlazorApp.Client.TodoModule.Domain.UseCases.Dispatcher;
using BlazorApp.Client.TodoModule.Domain.UseCases.Queries;
using BlazorApp.Client.TodoModule.Presentation.Views;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlazorApp.Client.Presentation
{
    public class TodoViewModel : ComponentBase
    {
        [Inject]
        public ICommandDispatcher CommandDispatcher { get; set; }

        [Inject]
        public IQueryDispatcher QueryDispatcher { get; set; }

        [Required(ErrorMessage = "Введите название Todo")]
        public string NewTodo { get; set; }

        public List<TodoDto> Items { get; set; }

        public TodoDto Selected { get; set; }

        protected override async Task OnInitAsync()
        {
            await LoadTodos();
            await base.OnInitAsync();
        }

        public async Task Create()
        {
            await CommandDispatcher.Execute(new Add(NewTodo));
            await LoadTodos();
            NewTodo = string.Empty;
        }

        public async Task Select(UIMouseEventArgs args)
        {
            var e = args as TodoTableComponent.ClickTodoEventArgs;
            if (e == null)
                return;
            var todo = await QueryDispatcher.Execute<GetById, TodoItem>(new GetById(e.Id));
            if (todo == null)
            {
                Selected = null;
                return;
            }
            Selected = new TodoDto { Id = todo.Id, IsComplite = todo.IsComplite, Name = todo.Name, Created = todo.Created };
        }

        public void CanselEdit()
        {
            Selected = null;
        }

        public async Task Update()
        {
            await CommandDispatcher.Execute(new Update(Selected.Id, Selected.Name, Selected.IsComplite));
            Selected = null;
            await LoadTodos();
        }

        private async Task LoadTodos()
        {
            var todos = await QueryDispatcher.Execute<GetAll, List<TodoItem>>(new GetAll());
            Items = todos.Select(t => new TodoDto { Id = t.Id, IsComplite = t.IsComplite, Name = t.Name, Created = t.Created })
                .ToList();
        }
    }
}
